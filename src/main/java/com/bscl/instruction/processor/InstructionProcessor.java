package com.bscl.instruction.processor;

public class InstructionProcessor {

	private InstructionValidator instructionValidator;
	private InstructionQueue<Instruction> instructionPriorityQueue;

	public void addInstruction(Instruction instruction) throws InvalidMessageException {
		boolean valid = instructionValidator.validate(instruction);

		if(!valid) {
			throw new InvalidMessageException();	
		}
		
		instructionPriorityQueue.add(instruction);
	}

	public int getNumberOfInstructionsOnTheQueue() {
		return instructionPriorityQueue.size();
	}
	
	public void removeInstructionsFromQueue() {
		instructionPriorityQueue.clear();
	}
	
	public Instruction getInstructionFromQueue() {
		return instructionPriorityQueue.poll();
	}

	public InstructionValidator getInstructionValidator() {
		return instructionValidator;
	}

	public void setInstructionValidator(InstructionValidator instructionValidator) {
		this.instructionValidator = instructionValidator;
	}


	public InstructionQueue<Instruction> getInstructionPriorityQueue() {
		return instructionPriorityQueue;
	}

	public void setInstructionPriorityQueue(InstructionQueue<Instruction> instructionPriorityQueue) {
		this.instructionPriorityQueue = instructionPriorityQueue;
	}



}
