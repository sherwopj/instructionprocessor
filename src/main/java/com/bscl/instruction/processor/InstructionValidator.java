package com.bscl.instruction.processor;

public class InstructionValidator {
	
	public boolean validate(Instruction instruction) {

		if(instruction.getInstructionType() < 1 || instruction.getInstructionType() > 99) {
			return false;
		}

		if(instruction.getProductCode() < 1) {
			return false;
		}
		
		if(instruction.getQuantity() < 1) {
			return false;
		}
		
		if(instruction.getTimeStamp() < 1) {
			return false;
		}
		
		return true;
	}

}
