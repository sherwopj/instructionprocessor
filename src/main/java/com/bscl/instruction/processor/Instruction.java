package com.bscl.instruction.processor;


public class Instruction implements Comparable<Instruction>{

	private int instructionType;
	private int productCode;
	private int quantity;
	private byte uom;
	private int timeStamp;

	public Instruction(int instructionType, int productCode, int quantity, byte uom, int timeStamp) {
		this.instructionType = instructionType;
		this.productCode = productCode;
		this.quantity = quantity;
		this.uom = uom;
		this.timeStamp = timeStamp;
	}


	public Instruction() {
		// TODO Auto-generated constructor stub
	}


	public int getInstructionType() {
		return instructionType;
	}

	public void setInstructionType(int instructionType) {
		this.instructionType = instructionType;
	}

	public int getProductCode() {
		return productCode;
	}

	public void setProductCode(int productCode) {
		this.productCode = productCode;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public byte getUom() {
		return uom;
	}

	public void setUom(byte uom) {
		this.uom = uom;
	}

	public int getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(int timeStamp) {
		this.timeStamp = timeStamp;
	}

	public int compareTo(Instruction otherInstruction) {
		return this.instructionType - otherInstruction.instructionType;
	}


	
}
