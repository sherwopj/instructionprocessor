package com.bscl.instruction.processor;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;

public class InstructionQueueTest {

	private InstructionQueue<Instruction> queue;
	private Instruction instructionWithHighPriority1;
	private Instruction instructionWithHighPriority2;
	private Instruction instructionWithHighPriority3;
	private Instruction instructionWithMediumPriority1;
	private Instruction instructionWithLowPriority1;

	@Before
	public void setUp() throws Exception {
		queue = new InstructionQueue<Instruction>();
		createSomeInstructions();
	}


	@Test
	public void shouldReturnMessagesInInstructionTypePriorityOrder() {
		//GIVEN
		queue.add(instructionWithHighPriority1);
		queue.add(instructionWithMediumPriority1);
		queue.add(instructionWithLowPriority1);
		
		//WHEN
		Instruction firstMessageRetrieved  = queue.poll();
		Instruction secondMessageRetrieved  = queue.poll();
		Instruction thirdMessageRetrieved  = queue.poll();
		
		//THEN
		assertThat("fisrt message retrieved from the queue should be for product code 101",firstMessageRetrieved.getProductCode(),is(101));
		assertThat("second message retrieved from the queue should be for product code 201",secondMessageRetrieved.getProductCode(),is(201));
		assertThat("third message retrieved from the queue should be for product code 301",thirdMessageRetrieved.getProductCode(),is(301));
	}
	
	@Test
	public void shouldReturnMessagesInInstructionTypePriorityOrderAndNotJustFIFO() {
		//GIVEN instructions are added lowest priority first
		queue.add(instructionWithLowPriority1);
		queue.add(instructionWithMediumPriority1);
		queue.add(instructionWithHighPriority1);
		
		//WHEN
		Instruction firstMessageRetrieved  = queue.poll();
		Instruction secondMessageRetrieved  = queue.poll();
		Instruction thirdMessageRetrieved  = queue.poll();
		
		//THEN
		assertThat("first message retrieved from the queue should be for product code 101",firstMessageRetrieved.getProductCode(),is(101));
		assertThat("second message retrieved from the queue should be for product code 201",secondMessageRetrieved.getProductCode(),is(201));
		assertThat("third message retrieved from the queue should be for product code 301",thirdMessageRetrieved.getProductCode(),is(301));
	}
	
	@Test
	public void shouldReturnTheNumberOfInstructionsCurrentlyOnTheQueue() {
		//GIVEN
		queue.add(instructionWithHighPriority1);
		queue.add(instructionWithHighPriority2);
		queue.add(instructionWithHighPriority3);
		queue.add(instructionWithMediumPriority1);
		//WHEN
		int numberOfInstructionsOnTheQueue = queue.size();
		//THEN
		assertThat("The correct total number of instructions are returned", numberOfInstructionsOnTheQueue, is(4));
	}
	
	@Test
	public void shouldAllowInstructionsToBeRemovedFromTheQueue() {
		//GIVEN
		queue.add(instructionWithHighPriority1);
		queue.add(instructionWithHighPriority2);
		queue.add(instructionWithHighPriority3);
		queue.add(instructionWithMediumPriority1);
		//WHEN
		queue.remove();
		int numberOfInstructionsOnTheQueue = queue.size();
		//THEN
		assertThat("The correct total number of instructions are returned", numberOfInstructionsOnTheQueue, is(3));
	}
	
	@Test
	public void shouldBeAbleToDetermineIfTheQueueIsEmpty() {
		//GIVEN
		//queue is new and empty
		//WHEN
		boolean emptyCheckResult = queue.isEmpty();
		//THEN
		assertThat("The queue is empty check returns true", emptyCheckResult, is(true));
	}
	

	private void createSomeInstructions() {
		instructionWithHighPriority1 = new Instruction(1,101,1,(byte)3,12345);
		instructionWithHighPriority2 = new Instruction(2,102,1,(byte)3,12345);
		instructionWithHighPriority3 = new Instruction(3,103,1,(byte)3,12345);
		
		instructionWithMediumPriority1 = new Instruction(11, 201, 1, (byte) 3, 12345);
		
		instructionWithLowPriority1 = new Instruction(91, 301, 1, (byte) 3, 12345);
	}

}
