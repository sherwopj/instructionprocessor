package com.bscl.instruction.processor;

import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class InstructionProcessorTest {
	
	private static final int NUMBER_OF_MESSAGES_CURRENTLY_ON_QUEUE = 4;

	private static final Boolean FAILED_VALIDATION = false;

	private static final Boolean PASSED_VALIDATION = true;

	@Mock
	private InstructionValidator mockValidator;
	
	@Mock
	private InstructionQueue<Instruction> mockQueue;

	@Mock
	private Instruction	mockInstruction;
	
	private InstructionProcessor instructionProcessor;



	@Before
	public void setUp() {
		instructionProcessor = new InstructionProcessor();
		instructionProcessor.setInstructionValidator(mockValidator);
		instructionProcessor.setInstructionPriorityQueue(mockQueue);
	}


	@Test
	public void shouldValidateInstructionBeforeAddingToTheQueue() throws InvalidMessageException {
		//GIVEN
		when(mockValidator.validate(mockInstruction)).thenReturn(PASSED_VALIDATION);
		//WHEN
		instructionProcessor.addInstruction(mockInstruction);
		
		//THEN
		 InOrder orderedExecution = inOrder(mockValidator, mockQueue);
		 orderedExecution.verify(mockValidator).validate(mockInstruction);
		 orderedExecution.verify(mockQueue).add(mockInstruction);
	}
	
	@Test(expected = InvalidMessageException.class)
	public void shouldThrowInvalidMessageExceptionIfValidationFails() throws InvalidMessageException {
		//GIVEN
		when(mockValidator.validate(mockInstruction)).thenReturn(FAILED_VALIDATION);
		
		//WHEN
		instructionProcessor.addInstruction(mockInstruction);
		
		//THEN
		//We expect an InvalidMessageException to be thrown
	}
	
	@Test
	public void shouldAddValidInstructionsToTheQueue() throws InvalidMessageException {
		//GIVEN
		when(mockValidator.validate(mockInstruction)).thenReturn(PASSED_VALIDATION);
		//WHEN
		instructionProcessor.addInstruction(mockInstruction);
		//THEN
		verify(mockQueue).add(mockInstruction);
	}
	
	@Test
	public void shouldReturnNumberOfInstructionsCurrentlyOnTheQueue() {
		//GIVEN
		when(mockQueue.size()).thenReturn(NUMBER_OF_MESSAGES_CURRENTLY_ON_QUEUE);
		//WHEN
		int numberOfInstructionsOnQueue = instructionProcessor.getNumberOfInstructionsOnTheQueue();
		//THEN
		Assert.assertEquals(NUMBER_OF_MESSAGES_CURRENTLY_ON_QUEUE, numberOfInstructionsOnQueue);
	}
	
	@Test
	public void shouldClearInstructionsFromQueueWhenRemoveMethodCalled() {
		//GIVEN
		//WHEN
		instructionProcessor.removeInstructionsFromQueue();
		//THEN
		verify(mockQueue).clear();
	}
	
	@Test
	public void shouldProvideMethodForGettingInstructionsFromTheTopOfTheQueue() {
		//GIVEN
		//WHEN
		instructionProcessor.getInstructionFromQueue();
		//THEN
		verify(mockQueue).poll();
		
	}
	
}
