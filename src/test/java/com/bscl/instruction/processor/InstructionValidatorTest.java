package com.bscl.instruction.processor;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;

public class InstructionValidatorTest {
	private Instruction instruction;
	private InstructionValidator instructionValidator;
	private boolean isValid;

//	InstructionType integer 0 < n < 100
//	ProductCode Integer 0 < n
//	Quantity Integer 0 < n
//	UOM byte 0 <= n < 256
//	TimeStamp Integer 0 < n
	
	@Before
	public void setUp() throws Exception {
		instructionValidator = new InstructionValidator();
		
		instruction = new Instruction();
		fillInstructionWithData(instruction);
	}


	@Test
	public void shouldSucessfullyValidateIfInstructionTypeIsMoreThanZero() {
		//GIVEN
		instruction.setInstructionType(1);
		//WHEN
		isValid = instructionValidator.validate(instruction);
		//THEN
        assertThat("Instruction Type should be more than zero and is", isValid, is(true));
	}

	@Test
	public void shouldFailValidationIfInstructionTypeIsLessThanOne() {
		//GIVEN
		instruction.setInstructionType(0);
		//WHEN
		isValid = instructionValidator.validate(instruction);
		//THEN
		assertThat("Instruction Type should be more than zero", isValid, is(false));
	}

	@Test
	public void shouldFailValidationIfInstructionTypeIsMoreThanNinetyNine() {
		//GIVEN
		instruction.setInstructionType(100);
		//WHEN
		isValid = instructionValidator.validate(instruction);
		//THEN
		assertThat("Instruction Type should be less than 100", isValid, is(false));
	}

	@Test
	public void shouldSucessfullyValidateIfProductCodeIsMoreThanZero() {
		//GIVEN
		instruction.setProductCode(12345);
		//WHEN
		isValid = instructionValidator.validate(instruction);
		//THEN
        assertThat("Product code should be more than zero and is", isValid, is(true));
	}

	@Test
	public void shouldFailValidationIfProductCodeIsLessThanOne() {
		//GIVEN
		instruction.setProductCode(0);
		//WHEN
		isValid = instructionValidator.validate(instruction);
		//THEN
		assertThat("Product code should be more than zero", isValid, is(false));
	}

	@Test
	public void shouldSucessfullyValidateIfQuantityIsMoreThanZero() {
		//GIVEN
		instruction.setQuantity(3);
		//WHEN
		isValid = instructionValidator.validate(instruction);
		//THEN
        assertThat("Product code should be more than zero and is", isValid, is(true));
	}

	@Test
	public void shouldFailValidationIfQuantityIsLessThanOne() {
		//GIVEN
		instruction.setQuantity(0);
		//WHEN
		isValid = instructionValidator.validate(instruction);
		//THEN
		assertThat("Quantity should be more than zero", isValid, is(false));
	}
	
	//'unsigned byte' MUST be 0 <= n < 256
	
	@Test
	public void shouldSucessfullyValidateIfTimeStampIsMoreThanZero() {
		//GIVEN
		instruction.setTimeStamp(1);
		//WHEN
		isValid = instructionValidator.validate(instruction);
		//THEN
        assertThat("Time Stamp should be more than zero and is", isValid, is(true));
	}

	@Test
	public void shouldFailValidationIfTimeStampIsLessThanOne() {
		//GIVEN
		instruction.setTimeStamp(0);
		//WHEN
		isValid = instructionValidator.validate(instruction);
		//THEN
		assertThat("Time Stamp should be more than zero", isValid, is(false));
	}

	

	private void fillInstructionWithData(Instruction instructionToBeFilled) {
		instructionToBeFilled.setInstructionType(5);
		instructionToBeFilled.setProductCode(12345);
		instructionToBeFilled.setQuantity(5);
		instructionToBeFilled.setUom(Byte.parseByte("16"));
		instructionToBeFilled.setTimeStamp(123);
	}

}
