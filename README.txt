Questions And Assumptions

1. Which elements of an instruction message are mandatory
Assumption:
	1.1 All fields are mandatory
	1.2 Messages could be validated prior to the 'InstructionProcessor' by some (web)service (WSDL/XSD say). Invalid messages should not be accepted so the sender gets a chance to correct 
	1.3 All fields are populated
2. Perhaps String would work better as a productCode
3. Instructions are part of the 'model' and will have been created prior to the InstructionProcessor being called
4. 'Has a method for removing InstructionMessages from the queue' clears the whole queue